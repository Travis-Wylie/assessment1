<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css" type="text/css">

  <link rel="shortcut icon" href="assets/favicon.ico">
  <link rel="icon" sizes="16x16 32x32 64x64" href="assets/favicon.ico">
  <link rel="icon" type="image/png" sizes="196x196" href="assets/favicon-192.png">
  <link rel="icon" type="image/png" sizes="160x160" href="assets/favicon-160.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon-96.png">
  <link rel="icon" type="image/png" sizes="64x64" href="assets/favicon-64.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon-16.png">

  <title>Tatenashi</title>
</head>

<body>
  <div class="content">
    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
      <img src="images/logo.png" style="height: 45px; padding-right: 10px;">
      <a class="navbar-brand" style="color:white">Tatenashi</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/docs.php">Docs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Servers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact Us</a>
          </li>
        </ul>

        <a class="btn btn-outline-success form-inline mt-2 mt-md-0" style="margin-right: 20px" href="https://discordapp.com/oauth2/authorize?client_id=277908880359686145&scope=bot&permissions=305261631"
          role="button">Invite to server</a>
        <a class="btn btn-outline-success form-inline mt-2 mt-md-0" href="https://discord.gg/gS6zZ3B " role="button">Join the Official Discord</a>

      </div>
    </nav>

    <div id="main">
      <div class="container col-lg-10 col-md-10 col-sm-10 col-xs-12">
        <h1>Tatenashi</h1>
        <h3>General Purpose Discord Bot</h3>
        <hr>
        <br>
        <div class="content">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse at turpis sed ipsum consequat semper. In dolor elit,
            mollis quis orci et, iaculis posuere elit. Ut interdum gravida mi sit amet ullamcorper. Pellentesque congue quis
            libero maximus placerat. Maecenas ac feugiat est. Proin lorem arcu, ultrices id eros tincidunt, porta facilisis
            quam. Sed quis ipsum quis mauris semper semper placerat a tellus. Duis vitae enim sit amet dolor lobortis dignissim.
            Integer massa nunc, pulvinar ut odio nec, porttitor tempor enim. Sed non sodales nisl, ac dignissim nunc. Sed
            placerat sed sapien vitae pharetra. Integer gravida tellus eget dictum laoreet. Morbi non diam vestibulum, malesuada
            magna a, dictum justo. Morbi vestibulum ut ante et tempor. Duis luctus volutpat nibh. Suspendisse tellus nisi,
            mollis sit amet tincidunt id, suscipit eu elit.
            <br>
            <br> Sed sed elit magna. Nam sagittis, neque convallis tempus commodo, augue neque tempus eros, sed ullamcorper libero
            dolor a tortor. Donec a tempus urna. Donec elit nisl, posuere a vestibulum quis, rhoncus vel ante. Pellentesque
            habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus lorem justo, ullamcorper
            eget molestie consectetur, hendrerit in enim. In tempus vitae libero a egestas. Ut tristique eros sit amet congue
            porttitor. Sed rutrum quis diam eget rutrum. Donec tincidunt placerat semper. Ut eget pulvinar lorem, ut vulputate
            ex. Etiam at varius dolor. Nunc et ante vel elit malesuada vehicula eget non velit. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.
          </p>


          <div class="section center">
            <hr>
            <h1>Official Discord and Twitter</h1>
            <a class="twitter-follow-button" href="https://twitter.com/Justiphi" data-size="large">
              Follow @Justiphi</a>
            <div class="container col-lg-8 col-md-10 col-sm-12 col-xs-12">
              <div class="row">
                <iframe src="https://discordapp.com/widget?id=288252004055384065&theme=dark" class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                  height=500px allowtransparency="false" frameborder="0" style="padding-top: 20px"></iframe>
                <div class="container col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top: 20px">
                  <a class="twitter-timeline" data-height="500px" data-theme="dark" href="https://twitter.com/JustiPhi?ref_src=twsrc%5Etfw">Tweets by JustiPhi</a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

    <div class="webs"></div>
  </div>

  <footer class="footer">
    <div class="container" style="justify-content:center;">
      <div style="display:float;">
        <p style="float:left;" class="footer-text text-muted">Justice♠ 2018</p>
      </div>
    </div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
    crossorigin="anonymous"></script>
</body>

</html>