CREATE DATABASE IF NOT EXISTS `tatenashi`;

USE `tatenashi`;


DROP TABLE IF EXISTS `commands`;

DROP TABLE IF EXISTS `sections`;

DROP TABLE IF EXISTS `servers`;

DROP TABLE IF EXISTS `contact`;

DROP TABLE IF EXISTS `headings`;



CREATE TABLE `sections` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `TITLE` TEXT DEFAULT NULL,
  `DESCRIPTION` TEXT DEFAULT NULL,
  `PAGE` TEXT DEFAULT NULL,	
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;

CREATE TABLE `commands` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `COMMAND` TEXT DEFAULT NULL,
  `DESCRIPTION` TEXT DEFAULT NULL,
  `PERMISSION` TEXT DEFAULT NULL,
  `SID` INT(11) NOT NULL,
  PRIMARY KEY (`ID`),
  FOREIGN KEY (`SID`) REFERENCES `sections`(`ID`)
) AUTO_INCREMENT=1;

CREATE TABLE `servers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `NAME` TEXT DEFAULT NULL,
  `INVITE` TEXT DEFAULT NULL,
  `WEBSITE` TEXT DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;

CREATE TABLE `contact` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` VARCHAR(255) DEFAULT NULL,
  `NAME` TEXT DEFAULT NULL,
  `SUBJECT` TEXT DEFAULT NULL,
  `COMMENT` TEXT DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;

CREATE TABLE `headings` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `TITLE` TEXT DEFAULT NULL,
  `SUBTITLE` TEXT DEFAULT NULL,
  `PAGE` TEXT DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;



INSERT INTO `sections` (`TITLE`,`DESCRIPTION`,`PAGE`) VALUES ("Cras eu","eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est.","home"),("morbi tristique","ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante","non"),("Sed eget lacus.","imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl elementum purus, accumsan interdum","home"),("ligula tortor,","tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam.","in"),("tristique senectus et","nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque","home"),("ipsum primis in","cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec","home"),("ante","gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum","Maecenas"),("arcu. Sed","sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel","facilisis"),("neque. Nullam nisl.","magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,","eget"),("lacus, varius et,","tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero.","nulla.");

INSERT INTO `commands` (`COMMAND`,`DESCRIPTION`,`PERMISSION`,`sID`) VALUES ("Sed dictum. Proin","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer","egestas",4),("ID risus quis","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur","egestas",2),("Aliquam rutrum lorem","Lorem ipsum dolor sit amet, consectetuer","iaculis",2),("Ut nec urna","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer","tellus",3),("a, scelerisque sed,","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur","fermentum",6),("a, enim. Suspendisse","Lorem ipsum dolor sit amet,","euismod",6),("tempus non, lacinia","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor.","luctus",6),("massa lobortis ultrices.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","Proin",3),("et, magna. Praesent","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur","tortor,",5),("libero. Integer in","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor.","Phasellus",6);

INSERT INTO `servers` (`NAME`,`INVITE`,`WEBSITE`) VALUES ("cursus purus. Nullam","ante","risus."),("Nulla aliquet.","a","Nunc"),("et,","eu","vel"),("vel, vulputate eu,","nisi","vel,"),("non dui nec","Proin","Duis"),("Cras interdum. Nunc","sodales.","nonummy"),("risus,","lorem,","quis"),("euismod","leo,","sapien."),("quis lectus.","et","sit"),("orci","neque.","mauris.");

INSERT INTO `contact` (`EMAIL`,`NAME`,`SUBJECT`,`COMMENT`) VALUES ("vehicula.et.rutrum@congue.com","luctus.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed","Lorem ipsum dolor sit"),("semper.erat.in@rhoncus.org","nulla vulputate dui,","Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis"),("ridiculus@odiotristiquepharetra.ca","eget nisi dictum","Lorem ipsum","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec"),("est.mollis.non@Quisquetinciduntpede.edu","ac, fermentum vel,","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus."),("ac@ipsum.net","enim","Lorem ipsum dolor sit","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor."),("vulputate.risus.a@sitamet.co.uk","quis","Lorem ipsum dolor sit amet, consectetuer adipiscing","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,"),("scelerisque.neque.sed@euduiCum.org","eu, placerat","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur","Lorem ipsum dolor sit amet,"),("cursus.et@posuere.org","erat","Lorem ipsum dolor","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,"),("a@nibh.org","porttitor scelerisque","Lorem ipsum dolor","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet"),("ut.sem@vulputate.org","sem mollis dui,","Lorem ipsum","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec");

INSERT INTO `headings` (`TITLE`,`SUBTITLE`,`PAGE`) VALUES ("fringilla euismod","eu dui. Cum sociis natoque","vitae"),("elit.","feugiat placerat velit. Quisque varius.","servers"),("Pellentesque ultricies dignissim","tempor, est ac mattis semper,","lacinia."),("erat eget","blandit enim consequat purus.","magna"),("non dui nec","tellus justo sit amet nulla.","home"),("orci.","lorem, vehicula et, rutrum eu,","tincidunt,"),("congue, elit","a, arcu. Sed et","ut,"),("Nunc","Curabitur ut odio","Curabitur"),("libero lacus, varius","cursus et, eros. Proin ultrices.","urna."),("velit eget","velit. Sed malesuada","condimentum");
