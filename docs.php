<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
        crossorigin="anonymous">
    <link rel="stylesheet" href="./css/styles.css" type="text/css">

    <link rel="shortcut icon" href="assets/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="assets/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="assets/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="assets/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="assets/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon-16.png">

    <title>Tatenashi</title>

</head>

<body>
    <div class="content">

        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <img src="images/logo.png" style="height: 45px; padding-right: 10px;">
            <a class="navbar-brand" style="color:white">Tatenashi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/index.php">Home</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/docs.php">Docs
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Servers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                </ul>

                <a class="btn btn-outline-success form-inline mt-2 mt-md-0" style="margin-right: 20px" href="https://discordapp.com/oauth2/authorize?client_id=277908880359686145&scope=bot&permissions=305261631"
                    role="button">Invite to server</a>
                <a class="btn btn-outline-success form-inline mt-2 mt-md-0" href="https://discord.gg/gS6zZ3B " role="button">Join the Official Discord</a>

            </div>
        </nav>

        <div class="sidenav" id="mySidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a href="#basic-commands">Basic</a>
            <a href="#moderation">Moderation</a>
        </div>

        <span class="sidenavmenu" onclick="openNav()">&#9776;</span>

        <div id="main">
            <div class="container col-lg-10 col-md-10 col-sm-10 col-xs-12">

                <h1>Documentation</h1>
                <h3>Version 6.12.23 -Rebirth</h3>
                <br>

                <div id="basic-commands" class="container-fluid section">
                    <hr>
                    <h2>Basic Commands</h2>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at porta turpis. Aliquam erat volutpat. Sed urna enim, ornare
                        eu enim ut, iaculis pulvinar metus. Phasellus vulputate ex quis odio luctus, eget finibus massa convallis.
                        Sed sit amet est quis nibh dapibus gravida sollicitudin quis justo. Sed nec aliquam risus. Ut vel
                        consequat mauris, nec viverra ipsum. Phasellus vitae dolor blandit, facilisis purus in, porta purus.
                        Vestibulum vehicula nibh vitae sapien ornare, porttitor finibus libero ornare.
                    </p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Command</th>
                                <th>Description</th>
                                <th>Required Permission</th>
                            </tr>
                            <tr>
                                <td>+help</td>
                                <td>Shows all commands</td>
                                <td>None</td>
                            </tr>
                            <tr>
                                <td>+ping</td>
                                <td>Bot replies Pong! with the ping</td>
                                <td>None</td>
                            </tr>
                            <tr>
                                <td>+coin</td>
                                <td>Flips a coin</td>
                                <td>None</td>
                            </tr>
                            <tr>
                                <td>+dice (number)</td>
                                <td>roles a (number) sided die</td>
                                <td>None</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="moderation" class="container-fluid section">
                    <hr>
                    <h2>Moderation Commands</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis est placerat tortor placerat feugiat. Proin malesuada
                        massa eget ullamcorper porta. In malesuada arcu et bibendum efficitur. Etiam dignissim magna a dapibus
                        viverra. Proin aliquam sodales felis, ac tincidunt leo imperdiet ac. Class aptent taciti sociosqu
                        ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque ut purus porttitor, scelerisque
                        dolor quis, vehicula lorem. Etiam varius, dui at mattis tempus, turpis est vestibulum massa, id faucibus
                        tellus eros in odio. Suspendisse vel ullamcorper mi. Nunc consequat quis leo auctor bibendum. Praesent
                        interdum eros vel consectetur convallis. Vestibulum sed ante urna. Integer odio lorem, suscipit sed
                        finibus non, vestibulum ac mi. Maecenas semper tincidunt nisi porttitor fermentum. Quisque eu est
                        bibendum, tristique nibh sed, pharetra ex. Integer in condimentum urna.
                    </p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Command</th>
                                <th>Description</th>
                                <th>Required Permission</th>
                            </tr>
                            <tr>
                                <td>+kick (@user)</td>
                                <td>kicks a user from the server</td>
                                <td>Kick users</td>
                            </tr>
                            <tr>
                                <td>+ban (@user)</td>
                                <td>bans a user from the server</td>
                                <td>Ban users</td>
                            </tr>
                            <tr>
                                <td>+purge (number)</td>
                                <td>deletes the last (number) messages</td>
                                <td>Manage messages</td>
                            </tr>
                            <tr>
                                <td>+setPrefix (prefix)</td>
                                <td>changes the bots prefix on the server to (prefix)</td>
                                <td>Administrator</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        <div class="webs"></div>
    </div>


    <footer class="footer">
        <div class="container" style="justify-content:center;">
            <div style="display:float;">
                <p style="float:left;" class="footer-text text-muted">Justice♠ 2018</p>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            // Add scrollspy to <body>
            $('body').scrollspy({ target: ".navbar", offset: 70 });

            // Add smooth scrolling on all links inside the navbar
            $("#mySidenav a").on('click', function (event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                }  // End if
            });
        });
    </script>


    <script>

        function openNav() {
            document.getElementById("mySidenav").style.width = "200px";
            document.getElementById("main").style.marginLeft = "200px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }
    </script>


</body>

</html>